---
title: "Los ecosistemas a traves del tiempo y el espacio"
author: "**César Hinojo Hinojo** <br> <br> Department of Ecology and Evolutionary Biology <br> The University of Arizona <br> chinojoh@arizona.edu"
date: "2020/11/25"
output: 
  xaringan::moon_reader:
    css: ["default", "default-fonts"]
---


# Un poco de mi historia

.center[
```{r echo=FALSE,out.width="100%"}
knitr::include_graphics(path = "universidades.png")
```
]

**Plantas y ecosistemas:** 

¿Cómo son?, ¿Qué hacen?*

---

# Intercambio gaseoso

.center[
```{r echo=FALSE,out.width="90%"}
knitr::include_graphics(path = "intercambio_gaseoso.png")
```
]


---

class: center, middle

.center[
```{r echo=FALSE,out.width="90%"}
knitr::include_graphics(path = "global_photosynthesis.jpg")
```
]


---
class: inverse
background-image: url("fotosintesis1.JPG")
background-position: center
background-size: contain

**Intercambio gaseoso: hojas**


---
class: inverse
background-image: url("fotosintesis2.JPG")
background-position: center
background-size: contain


---
class: center, middle

# Curva de respuesta de la fotosintesis al CO<sub>2</sub>

```{r echo=FALSE,out.width="90%"}
knitr::include_graphics(path = "AvsCi.png")
```


---

# Hojas desérticas, super fotosintéticas

**¿Por qué?**

Mucho nitrógeno, 41-52% invertido en fotosíntesis

```{r echo=FALSE,out.width="60%"}
knitr::include_graphics(path = "desierto_fotosintesis1.png")
```

---
# Hojas deserticas, super fotosintéticas

**¿Y eso qué?**

Les permite sobrevivir y capturar bastante CO<sub>2</sub>

```{r echo=FALSE,out.width="100%"}
knitr::include_graphics(path = "desierto_fotosintesis2.png")
```


---
background-image: url("churiN.jpg")
background-position: center
background-size: contain

**Intercambio gaseoso: Ecosistema**

Eddy covariance


---

# Eddy covariance mide cada 30 minutos

```{r echo=FALSE,out.width="100%"}
knitr::include_graphics(path = "eddy_data.png")
```

---
# Torres en el desierto

.pull-left[
Matorral

```{r echo=FALSE,out.width="100%"}
knitr::include_graphics(path = "churiN3.JPG")
```
]

.pull-right[
Pastizal

```{r echo=FALSE,out.width="100%"}
knitr::include_graphics(path = "buffel.JPG")
```
]

---
class: inverse
background-image: url("instalando1.jpg")
background-position: center
background-size: contain

---
class: inverse
background-image: url("instalando2.jpg")
background-position: center
background-size: contain

---
class: inverse
background-image: url("instalando3.jpg")
background-position: center
background-size: contain

---
class: inverse
background-image: url("instalando5.jpg")
background-position: center
background-size: contain

---
class: inverse
background-image: url("instalando6.jpg")
background-position: center
background-size: contain

---
class: inverse
background-image: url("instalando7.jpg")
background-position: center
background-size: contain

---

# Intercambio de CO<sub>2</sub>, ecosistemas desérticos 

En 3 años, ambos fijan $\approx$ 400 g C m<sup>-2</sup>
¿Cómo es posible?

```{r echo=FALSE,out.width="100%"}
knitr::include_graphics(path = "comparacion_churi.png")
```

---
class: inverse, center, middle

# Intercambio gaseoso: región <br> Modelos de la superficie terrestre vs Satelites


---

# Imágenes satelitales

.center[
```{r echo=FALSE,out.width="65%"}
knitr::include_graphics(path = "reflectancia.png")
```
]

Reflectancia -> características de la vegetación

---

# Índices de vegetación

.center[
```{r echo=FALSE,out.width="70%"}
knitr::include_graphics(path = "VIs.png")
```
]

.pull-left[
.center[
$NDVI = \frac{NIR - Rojo}{NIR + Rojo}$

Correlacionado con fotosíntesis

]
]

.pull-right[
.center[
$NIR_{v} = NDVI \cdot NIR$

Correlacion aún mejor

¿Por qué?
]
]

---

# Importancia del infrarojo cercano

Fotosintesis $\approx f$(cantidad de hojas, fotosintesis hoja)

.center[
```{r echo=FALSE,out.width="100%"}
knitr::include_graphics(path = "GPPvsVIs.png")
```
]

<br>
.center[
NIR<sub>v</sub> es sensible al grosor y nitrogeno de la hoja
]


---
class: inverse, center, middle
background-image: url("spruce.jpg")
background-position: center
background-size: contain

# ¿Qué esta pasando en la Taiga?



---

# Fuego y sucesion

.center[
```{r echo=FALSE,out.width="70%"}
knitr::include_graphics(path = "fire_succession_diagram_HH.png")
```
]

.pull-left[
```{r echo=FALSE,out.width="90%"}
knitr::include_graphics(path = "fig3_landsat_timeseries_noDecade_v3_6_forPaper.png")
```
]


.pull-right[
```{r echo=FALSE,out.width="90%"}
knitr::include_graphics(path = "canada_NIRv_slope.png")
```
]



---
class: inverse, center, middle
background-image: url("drought_sens.jpg")
background-position: center
background-size: contain

# Ecosistemas montañosos de Colorado


---

class: middle 

# Reflexion

- Intercambio gaseoso: complejo y dinámico

<br>
- Hay muchos huecos

<br>
- "¿Depende de la especie?", ¿por qué?

<br>
- Hay muchas oportunidades (ciencia, manejo, politica, economía, etc)


---

# Recursos

Datos:

- TRY: Características de las especies
- Fluxnet: Torres del mundo
- VegBank: Especímenes, vegetación, características
- ECOSIS: Datos espectrales
- DataONE: Descubridor de datos
- Satelites: Landsat, MODIS, Sentinel, etc.
- NEON: Todo... solo EUA

Modelos

Teoría

Computación: software, supercomputadoras, etc.


---
class: center, middle

```{r echo=FALSE,out.width="100%"}
knitr::include_graphics(path = "gracias.png")
```
