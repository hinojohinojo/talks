---
title: Linking remote sensing, plant traits and ecosystem processes
author: Cesar Hinojo Hinojo
date: July 30, 2020
output:
  revealjs::revealjs_presentation:
    theme: solarized
    transition: none
    highlight: default
    reveal_options:
      slideNumber: true
---

# My background

Plant ecophysiologist


Bachelor to Phd - Universidad de Sonora. Postdoc - UC Irvine

*How plants regulate the rate of carbon and water fluxes?*

![research](img/research.png){ width=45% }


# Why vegetation indices are correlated with gross primary production?

# Vegetation indices

![reflectance](img/refPlot.png){ width=40% }

**$NDVI$**$=\frac{NIR-Red}{NIR+Red}$, **$NIR_{v}$**$=NDVI * NIR$,

**$EVI$**$=\frac{NIR-Red}{NIR+6Red-7.5Blue+1}$


# Which one is better predictor of GPP?

Eddy covariance vs MODIS data. Several biomes

![VIvsGPP](img/VIvsGPP.png){ width=53% }


# Why their performance differs?

NIRv and EVI better capture the relevant processes

![VIvsGPPmodels](img/VIvsGPPmodels.png){ width=60% }


# Are boreal forest declining?

Papers say vegetation indices (NDVI) are declining

![succession](img/fire_succession_diagram_HH.png){ width=45% }

![chrono and artmo](img/chronosequence_rasterPlots.png){ width=65% }

Fires and succession. Links between VIs and processes.


# Ideas

- Which traits better predict ecosystem processes and why? At what scale?
- How to integrate trait-based approaches with other approaches?
- Are we missing important traits?


