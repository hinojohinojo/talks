---
title: "Assessing leaf mass per area across biomes and its variation in time"
author: "César Hinojo Hinojo"
date: "2021/04/02"
output: 
  xaringan::moon_reader:
    css: ["default", "default-fonts"]
---

<!--
Introduction
Climate change and biodiversity
Two examples, shifts in elevation ranges, forest dieback

What do we know about how climate influence traits, Leaf level analyses and community level analyses, a lack of temporal changes.

An oportunity for remote sensing (hyperspectral vs multispectral)

Questions
How can we retrieve community LMA from multispectral satellites that works across several biomes?
How has LMA changed over the last 3 decades across biomes?
what is driving such changes?

Following the spatial patterns, as climate gets warmer and drier, a generalized shift towards higher LMA may be expected.


-->

# Climate and biodiversity

Examples of climate-related biodiversity shifts

.pull-left[
Widespread shifts in elevation ranges

```{r echo=FALSE,out.width="90%"}
knitr::include_graphics(path = "range_shifts.jpg")
```

From Kelly & Goulden (2008)
]

.pull-right[
Widespread forest decline

```{r echo=FALSE,out.width="90%"}
knitr::include_graphics(path = "forest_death.JPG")
```
]

.center[
Composition -> **traits** -> ecosystem function
]

---

# Traits changing with climate

Leaf mass per area (LMA) - a key trait

From spatial analyses (how climate influence LMA across space?)...

.pull-left[

**Leaf level analyses**

- Weak influence of climate
- LMA decrease with temperature
- Influence of soil properties

Moles et al 2014, Ordoñez et al 2009

]

.pull-right[

**Community-level analyses**

- LMA decrease with temperature, VPD and PET
- Climate influence varies among broad biome types and soil

Bagousse-Pinguet et al 2017, Simova et al 2018, Wieczynsky et al 2019, 

]

<br />
.center[
**But... There is no temporal dataset across biomes**
]

---

# Using satellite remote sensing

.center[
```{r echo=FALSE,out.width="65%"}
knitr::include_graphics(path = "reflectancia.png")
```

Multispectrals with global coverage since 80s 
]

<!--
El articulo de NEON de Townsend tiene r2 de 0.77
-->

---

# Goals, questions and hypotheses

*Design an approach to track community mean LMA from multispectral satellites*

1. How has LMA changed over the last 3 decades across biomes?
2. What are the main drivers and factors influencing this change?

... as climate gets warmer and drier -> generalized decrease in LMA, but with differing response among biomes

---

class: center, middle, inverse

# Designing and testing iLMA


---

# A basis from radiative transfer models

PROSAIL: Model reflectance from leaf and canopy properties

- Leaf: Chlorophyll, Water, Mesophyll, LMA
- Canopy: Leaf area index (LAI), leaf angles
- Other: Soil reflectance, sun and view geometries...

.center[
Sensitivity analyses
```{r echo=FALSE,out.width="85%"}
knitr::include_graphics(path = "GSA_side_by_side.png")
```
]


---

# iLMA


.center[
```{r echo=FALSE,out.width="70%"}
knitr::include_graphics(path = "iLMA_logic.png")
```
<br />
<br />

$$iLMA = \frac{NDMI + c}{NIR_{v}}$$
]

---

# Testing with datasets

**RMBL**: elevation gradient, traits weighted by abundance

**NEON**: Sites across US, most major biomes, Combining vegetation and trait plots. Weighted by basal area.

**Forest database**: Sites across America with samples of vegetation and traits. Weighted by basal area.

Gathered Landsat 5 - 8 data for each site/plot

---

# Test at RMBL sites

.center[
```{r echo=FALSE,out.width="90%"}
knitr::include_graphics(path = "LMA_and_VI_boxplots_moreData.png")
```

iLMA capture LMA changes across the elevational gradient
]

---

# Test at RMBL sites

.center[
```{r echo=FALSE,out.width="80%"}
knitr::include_graphics(path = "VIs_vs_LMA_moreData_realLMA.png")
```
]

---

# Test at NEON sites

.center[
```{r echo=FALSE,out.width="80%"}
knitr::include_graphics(path = "iLMA_vs_LMA_at_NEON_sites_vegType.png")
```

A recent hyperspectral estimate at NEON: r2 = 0.77
]

---

# Test from forest database

.center[
```{r echo=FALSE,out.width="90%"}
knitr::include_graphics(path = "forests_iLMAvsLMA.png")
```
]

---

class: center, middle, inverse

# Temporal changes in LMA

---

# LMA changes at RMBL

.center[
```{r echo=FALSE,out.width="75%"}
knitr::include_graphics(path = "rmbl_LMA_temporal_changes.png")
```
<br />

LMA increasing at several sites
]

---

# LMA changes at NEON sites

.center[
```{r echo=FALSE,out.width="80%"}
knitr::include_graphics(path = "NEON_LMA_temporal_changes.png")
```

LMA decreasing mostly at evergreen and mixed forest
]

---

# An example with SOAP site

Conifers at this site are declining since severe droughts after 2010

.center[
2018

```{r echo=FALSE,out.width="70%"}
knitr::include_graphics(path = "NEON.D17.SOAP.DP1.00033_2018_06_08_120006.jpg")
```
]

---

# An example with SOAP site

Conifers at this site are declining since severe droughts after 2010

.center[
2019

```{r echo=FALSE,out.width="70%"}
knitr::include_graphics(path = "NEON.D17.SOAP.DP1.00033_2019_06_04_120006.jpg")
```
]

---

# An example with SOAP site

Conifers at this site are declining since severe droughts after 2010

.center[
2020

```{r echo=FALSE,out.width="70%"}
knitr::include_graphics(path = "NEON.D17.SOAP.DP1.00033_2020_06_08_120008.jpg")
```
]

---

# Some final comments

- iLMA capture broad patterns of LMA across biomes
- In meadows: shifts toward more conservative strategies?
- In forests: decline in trees and/or more understory
- Drivers? Temperature and droughts?
